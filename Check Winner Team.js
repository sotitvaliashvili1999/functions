const teams = [];
teams[0] = {
  name: "Power Rangers",
  scores: [44, 32, 71],
  statistic: {
    average: 0,
  },
};
teams[1] = {
  name: "Fairy Tails",
  scores: [65, 54, 49],
  statistic: {
    average: 0,
  },
};

//  1.1. fucnt. calcAverage;
function calcAverage(teams) {
  for (const team of teams) {
    let avg = 0;
    for (const score of team.scores) {
      avg += score / team.scores.length;
    }
    team.statistic.average = avg;
  }
}
//  1.2.
calcAverage(teams);

//  1.3. funct. checkWinner(avg1, avg2);
function checkWinner(x, y) {
  if (x > 2 * y) console.log(`Power Rangers is winner, ${x} vs ${y} score`);
  else if (y > 2 * x) console.log(`Fairy Tails is winner, ${y} vs ${x} score`);
  else console.log("D R A W !!!");
}
// 1.4
checkWinner(teams[0].statistic.average, teams[1].statistic.average);

// 2) PowerRangers (85, 54, 41) , FairyTails (23, 34, 47);
teams[0].scores.splice(0, 3, 85, 54, 41);
teams[1].scores.splice(0, 3, 23, 34, 47);
calcAverage(teams);
checkWinner(teams[0].statistic.average, teams[1].statistic.average);
