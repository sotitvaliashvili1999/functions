// 2.1. funct. of calc Tea Fee;
function calcTip(x) {
  if (x > 50 && x < 300) {
    return x * (3 / 20);
  } else {
    return x * (1 / 5);
  }
}
// 2.6. funct. calc averages;
function calcAvgFee(object) {
  for (const x of object.restaurantFee) {
    object.statistic.avgTip += x.tip / object.restaurantFee.length;
    object.statistic.avgTotal += x.total / object.restaurantFee.length;
  }
}
const duqnisLomi = {
  firstName: "Ludoviko",
  restaurantFee: [
    { fee: 22, tip: 0, total: 0 },
    { fee: 295, tip: 0, total: 0 },
    { fee: 176, tip: 0, total: 0 },
    { fee: 440, tip: 0, total: 0 },
    { fee: 37, tip: 0, total: 0 },
    { fee: 105, tip: 0, total: 0 },
    { fee: 10, tip: 0, total: 0 },
    { fee: 1100, tip: 0, total: 0 },
    { fee: 96, tip: 0, total: 0 },
    { fee: 52, tip: 0, total: 0 },
  ],
  statistic: {
    avgTip: 0,
    avgTotal: 0,
  },
};
//2.3. calc TeaFees by using funct;
for (const x of duqnisLomi.restaurantFee) {
  x.tip = calcTip(x.fee);
}
//2.4. total;
for (const x of duqnisLomi.restaurantFee) {
  x.total = x.fee + x.tip;
}
// 2.6.
calcAvgFee(duqnisLomi);
console.log(duqnisLomi.statistic.avgTip);
